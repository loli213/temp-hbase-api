package com.group.name.model.response;

/**
 * Represents the data to send back to the client.
 * Contains the path to the HDFS folder that the results will be saved into.
 *
 */
public class ScanResponseModelData {
    private String futureResultPath;
    private String email;
    private String urlToNotify;
    private String msg;

    public ScanResponseModelData(String resultPath, String email, String urlToNotify, String msg) {
        this.futureResultPath = resultPath;
        this.email = email;
        this.urlToNotify = urlToNotify;
        this.msg = msg;
    }

    public String getResultPath() {
        return futureResultPath;
    }

    public void setResultPath(String resultPath) {
        this.futureResultPath = resultPath;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrlToNotify() {
        return urlToNotify;
    }

    public void setUrlToNotify(String urlToNotify) {
        this.urlToNotify = urlToNotify;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
