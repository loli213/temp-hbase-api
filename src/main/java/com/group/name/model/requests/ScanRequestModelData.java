package com.group.name.model.requests;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Represents an object the the client sends to us while sending scan post request.
 */
public class ScanRequestModelData {

    // request body fields
    @NotNull(message = "username cannot be null. Please specify your username")
    private String username;


    @NotNull
    private String tableName;

    // todo - validation that urlToNotify && emailToNotify not null. Each one can be null, but not both - DONE
    private String urlToNotify; // url address to be called with post request when the scan is over.

    @NotNull(message = "username cannot be null. Please specify your email to get a notification when the scan is over")
    @Email // Email validation.
    // todo - add 'send-email' option: https://stackoverflow.com/questions/44405168/how-to-send-mail-via-outlook-using-spring-boot
    private String emailToNotify; // an email will be sent to this address when the scan is over.

    private List<String> selectedColumns;

    private List<String> selectedPrefixColumns;

    private List<String> selectedValues;

    @NotNull(message = "startTimestamp cannot be null. Please specify startTimestamp")
    private String startTimestamp;

    @NotNull(message = "endTimestamp cannot be null. Please specify endTimestamp")
    private String endTimestamp;

    private String startRow;

    private String endRow;


    // section of getters & setters:
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getUrlToNotify() {
        return urlToNotify;
    }

    public void setUrlToNotify(String urlToNotify) {
        this.urlToNotify = urlToNotify;
    }

    public String getEmailToNotify() {
        return emailToNotify;
    }

    public void setEmailToNotify(String emailToNotify) {
        this.emailToNotify = emailToNotify;
    }


    public String getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(String startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public String getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(String endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public String getStartRow() {
        return startRow;
    }

    public void setStartRow(String startRow) {
        this.startRow = startRow;
    }

    public String getEndRow() {
        return endRow;
    }

    public void setEndRow(String endRow) {
        this.endRow = endRow;
    }

    public List<String> getSelectedColumns() {
        return selectedColumns;
    }

    public void setSelectedColumns(List<String> selectedColumns) {
        this.selectedColumns = selectedColumns;
    }

    public List<String> getSelectedPrefixColumns() {
        return selectedPrefixColumns;
    }

    public void setSelectedPrefixColumns(List<String> selectedPrefixColumns) {
        this.selectedPrefixColumns = selectedPrefixColumns;
    }

    public List<String> getSelectedValues() {
        return selectedValues;
    }

    public void setSelectedValues(List<String> selectedValues) {
        this.selectedValues = selectedValues;
    }
}
