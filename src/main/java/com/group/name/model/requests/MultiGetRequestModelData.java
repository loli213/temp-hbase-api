package com.group.name.model.requests;

import com.group.name.model.validation.MultiGetValidation;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@MultiGetValidation(username = "username", rowKeys = "rowKeys", message = "No more get requests are allowed")
public class MultiGetRequestModelData {
    /**
     * Json of the Http post request for example:
     * {
     * "tableName": "value 1",
     * "username": 123,
     *
     * "rowKeys": [
     * "row_key_22",
     * "row_key_23",
     * "row_key_24"
     * ]
     *
     * }
     */

    @NotNull(message = "tableName can't be null")
    private String tableName;

    @NotNull(message = "username can't be null")
    private String username; // TODO: add salt to username& password table, save hash(password || salt) as the password. Consider to add pepper (A-Z).


    @NotNull(message = "rowKeys can't be null")
    @NotEmpty(message = "rowKeys is empty")
    private List<String> rowKeys;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getRowKeys() {
        return rowKeys;
    }

    public void setRowKeys(List<String> rowKeys) {
        this.rowKeys = rowKeys;
    }
}
