package com.group.name.model.bl;

import com.group.name.db_connector.impl.DBUpdater;
import com.group.name.model.requests.GetRequestModelData;
import com.group.name.model.response.GetResponseModelData;
import com.group.name.model.utils.HBaseConnectionSetup;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellScanner;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Business logic of handling get request from the client.
 * Contains the limit & Updating the DB
 */
public class GetRequestBL {

    // TODO - consider what to do when there is a need to send the password in the get request (security reasons).
    public GetResponseModelData get(GetRequestModelData getRequestModelData, boolean updateDB) throws IOException {
        String tableName = getRequestModelData.getTableName();
        String rowKey = getRequestModelData.getRowKey();
        String username = getRequestModelData.getUsername();

        if (updateDB) {
            DBUpdater dbUpdater = DBUpdater.getInstance();
            int effectedRows = dbUpdater.updateRequestNum(1, username); // update DB.
            System.out.println("Was DB Update succeed? " + (effectedRows == 1));
        }

        Connection connection = HBaseConnectionSetup.getConnection();
        Table table = connection.getTable(TableName.valueOf(tableName));

        Map<String, String> columnNameToValueMap = new HashMap<>();
        Get getRequest = new Get(rowKey.getBytes());
        Result result = table.get(getRequest);

        CellScanner cellScanner = result.cellScanner();
        Cell currentCell;
        String currentQualifier, currentFamily, currentValue, putFormat = "%s:%s";
        while (cellScanner.advance()) {
            currentCell = cellScanner.current();
            currentFamily = new String(CellUtil.cloneFamily(currentCell));
            currentQualifier = new String(CellUtil.cloneQualifier(currentCell));
            currentValue = new String(CellUtil.cloneValue(currentCell));
            System.out.println(String.format("currentFamily=%s, currentQualifier=%s, value = %s", currentFamily, currentQualifier, currentValue));
            columnNameToValueMap.put(String.format(putFormat, currentFamily, currentQualifier), currentValue);
        }
        table.close();
        //connection.close(); - commented out in order to avoid setup connection every request.
        return new GetResponseModelData(rowKey, columnNameToValueMap);
    }

}
