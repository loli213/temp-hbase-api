package com.group.name.model.validation;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class MultiGetValidator implements ConstraintValidator<MultiGetValidation, Object> {

    @Autowired
    private UserRequestAllow userRequestAllow;

    private String username;
    private String rowKeys;


    @Override
    public void initialize(MultiGetValidation constraintAnnotation) {
        username = constraintAnnotation.username();
        rowKeys = constraintAnnotation.rowKeys();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        BeanWrapperImpl beanWrapper = new BeanWrapperImpl(o);
        try {
            String usernameValue = (String) beanWrapper.getPropertyValue(username);
            List temp = (List) beanWrapper.getPropertyValue(rowKeys);
            if (temp != null) {
                List<String> rowKeysValue = Lists.newArrayList(
                        Iterables.filter(temp, String.class));
                return userRequestAllow.isValid(usernameValue, rowKeysValue.size());
            } else {
                System.out.println("Failed to cast the list");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
