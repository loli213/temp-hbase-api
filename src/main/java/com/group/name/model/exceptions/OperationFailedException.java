package com.group.name.model.exceptions;

public class OperationFailedException extends RuntimeException {

    public OperationFailedException(String msg) {
        super(msg);
    }
}
