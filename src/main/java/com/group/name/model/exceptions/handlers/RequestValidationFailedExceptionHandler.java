package com.group.name.model.exceptions.handlers;

import com.group.name.model.exceptions.HeaderValidationFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class RequestValidationFailedExceptionHandler extends RuntimeException{

    @ExceptionHandler(value = {ConstraintViolationException.class})
    public ResponseEntity<List<String>> handleRequestValidationException(ConstraintViolationException exception) {
        List<String> errorMessages = new ArrayList<>();
        HttpStatus responseStatusCode = HttpStatus.INTERNAL_SERVER_ERROR;
        for (ConstraintViolation constraintViolation : exception.getConstraintViolations()) {
            errorMessages.add(constraintViolation.getMessage());
        }

        if (errorMessages.contains("No more get requests are allowed")) {
            responseStatusCode = HttpStatus.TOO_MANY_REQUESTS; // the user sent too many requests.
        }
        return new ResponseEntity<>(errorMessages, responseStatusCode);
    }

    @ExceptionHandler(value = {HeaderValidationFailedException.class})
    public ResponseEntity<String> handleHeaderValidationException(HeaderValidationFailedException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }



}
