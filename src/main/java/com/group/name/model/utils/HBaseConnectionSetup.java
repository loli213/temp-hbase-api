package com.group.name.model.utils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

/**
 * Setup connection to HBase.
 */
public class HBaseConnectionSetup {

    private static Connection connection = setupConnection();

    private HBaseConnectionSetup(){}

    // hbase url: http://localhost:16010
    private static Connection setupConnection() {
        /*Configuration config = HBaseConfiguration.create();
        config.clear();
        config.set("hbase.zookeeper.quorum", "localhost");
        config.set("hbase.zookeeper.property.clientPort","2181");
        config.set("hbase.master", "localhost:60000");*/
        Configuration conf = HBaseConfiguration.create();
        Connection conn = null;
        try {
            conn = ConnectionFactory.createConnection(conf);
            System.out.println("HBase connection was created");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to setup HBase Connection");
        }
        return conn;
    }

    public static Connection getConnection() {
        return connection;
    }
}
