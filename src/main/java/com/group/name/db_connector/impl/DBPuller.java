package com.group.name.db_connector.impl;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Read data from the DB.
 */
@Component
public class DBPuller {

    private PreparedStatement preparedStatement;


    public DBPuller() throws SQLException {
        Connection connection = HikariCPConnection.getConnection();
        this.preparedStatement = connection.prepareStatement(String.format("SELECT * from %s WHERE username=?", DBDefinitions.TABLE_NAME));
    }

    public ResultSet getRow(String username) {
        ResultSet resultSet = null;
        try {
            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Returned ResultSet=null");
        }
        return resultSet;
    }


}

