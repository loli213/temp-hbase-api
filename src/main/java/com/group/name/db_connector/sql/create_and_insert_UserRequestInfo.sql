-- use the needed schema.
use hbase;

-- create table named UserRequestInfo
CREATE TABLE IF NOT EXISTS UserRequestInfo (
	username TINYTEXT NOT NULL,
    requestsPerDay INT UNSIGNED NOT NULL,
    recordsPerDay INT UNSIGNED NOT NULL,
    allowedRecordsPerDay INT UNSIGNED NOT NULL,
    allowedRequestsPerDay INT UNSIGNED NOT NULL,
    totalRequests BIGINT UNSIGNED NOT NULL,
    totalRecords BIGINT UNSIGNED NOT NULL,
    totalAllowedRecords BIGINT UNSIGNED NOT NULL,
    totalAllowedRequests BIGINT UNSIGNED NOT NULL,
	lastGet TIMESTAMP NOT NULL
);


-- In a case you got an error with loading data from a csv file use the follow:
SET GLOBAL local_infile = true;
SHOW GLOBAL VARIABLES LIKE 'local_infile';

-- Load data to the table, from a csv file.
-- assume that the first line contains the column names.
LOAD DATA LOCAL INFILE  'C:\Users\bob\Downloads\userRequestInfoData.csv'
INTO TABLE UserRequestInfo
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 2 ROWS; -- header & column names.

-- show table's data
select * from userrequestinfo;

-- An example for inserting a row
INSERT INTO UserRequestInfo
VALUES ('bob123', '5', '5', '10', '10', '5', '5', '20', '15','2019-04-12T00:00:00');