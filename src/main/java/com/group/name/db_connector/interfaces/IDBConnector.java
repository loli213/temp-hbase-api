package com.group.name.db_connector.interfaces;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * An interface for get a connection to the DB.
 */
// todo - remove it?
public interface IDBConnector {

    /**
     * @return the connection to the DB.
     */
    static Connection getConnection() throws SQLException {
        return null;
    }
}
